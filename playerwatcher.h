#ifndef PLAYERWATCHER_H
#define PLAYERWATCHER_H

#include "definitions.h"
#include <QObject>
#include <QSet>

class PlayerWatcher : public QObject
{
    Q_OBJECT
public:
    enum class EventType
    {
        LoginEvent,
        LogoutEvent
    };

    enum class RelationType
    {
        Friend,
        Enemy,
        Unrelated
    };

    explicit PlayerWatcher(QObject *parent = 0);

    typedef QList<std::tuple<PlayerWatcher::EventType, PlayerWatcher::RelationType, QStringList>> EventDataType;

signals:
    void eventFired(PlayerWatcher::EventDataType);

public slots:
    void execute(PlayerList data);

    void setFriends(PlayerList data);
    void setEnemies(PlayerList data);

private:
    typedef QSet<QString> PlayerCollection;

    PlayerCollection friends;
    PlayerCollection enemies;
    PlayerCollection all;
};

#endif // PLAYERWATCHER_H
