#ifndef PLAYERSORTMODEL_H
#define PLAYERSORTMODEL_H

#include "definitions.h"

#include <QObject>
#include <QSet>
#include <QSortFilterProxyModel>

class PlayerSortModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit PlayerSortModel(QObject * parent);

    enum SortRole
    {
        RelationSortRole = Qt::UserRole
    };

    struct FilterLimits
    {
        FilterLimits()
        {
            this->friends = true;
            this->enemies = true;
            this->others = true;
        }

        FilterLimits(const FilterLimits& that) = default;
        FilterLimits & operator=(const FilterLimits&) = default;

        bool operator==(const FilterLimits& other){
            return (other.enemies == this->enemies) && (other.friends == this->friends) && (other.others == this->others);
        }

        bool friends;
        bool enemies;
        bool others;
    };

    void setFilterLimits(FilterLimits limit);
    PlayerSortModel::FilterLimits filterLimits();

public slots:
    void setFriends(PlayerList data);
    void setEnemies(PlayerList data);

    // QSortFilterProxyModel interface
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;

private:
    PlayerSortModel::FilterLimits limit;

    QSet<QString> friends;
    QSet<QString> enemies;
};

#endif // PLAYERSORTMODEL_H
