#include "urlvalidationstrategy.h"
#include <QUrl>

bool UrlValidationStrategy::check(const QString &url) const
{
    QUrl validator;
    validator.setUrl(url, QUrl::StrictMode);
    return validator.isValid();
}
