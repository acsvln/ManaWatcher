#ifndef INMEMORYSTORAGE_H
#define INMEMORYSTORAGE_H

#include <QVariant>
#include <QMap>

class InMemoryStorage
{
public:
    void clear();
    bool contains(const QString &key) const;
    void setValue(const QString &key, const QVariant &value);
    QVariant value(const QString &key, const QVariant &defaultValue = QVariant()) const;
private:
    QMap<QString, QVariant> storage;
};

#endif // INMEMORYSTORAGE_H
