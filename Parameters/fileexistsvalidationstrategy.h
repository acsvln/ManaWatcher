#ifndef FILEEXISTSVALIDATIONSTRATEGY_H
#define FILEEXISTSVALIDATIONSTRATEGY_H

#include <QString>

class FileExistsValidationStrategy
{
public:
    bool check(const QString & fileName) const;
};

#endif // FILEEXISTSVALIDATIONSTRATEGY_H
