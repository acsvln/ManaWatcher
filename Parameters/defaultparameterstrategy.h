#ifndef DEFAULTPARAMETERSTRATEGY_H
#define DEFAULTPARAMETERSTRATEGY_H

#include <QSettings>
#include <QString>
#include <QVariant>

template<typename Type>
class NoneValidationStrategy
{
public:
    bool check(const Type&) const
    {
        return true;
    }
};

template<typename Type,
         typename ValidationStrategy = NoneValidationStrategy<Type>,
         typename Storage = QSettings>
class DefaultParameterStrategy
{
public:
    DefaultParameterStrategy(ValidationStrategy&& validationStrategy = ValidationStrategy()):
        validationStrategy(std::move(validationStrategy))
    {

    }

    Type load(Storage & settings, const QString & name, const Type& defaultValue) const
    {
        QVariant rawValue = settings.value(name, defaultValue);

        bool ok = rawValue.canConvert<Type>();

        if (!ok)
            rawValue = defaultValue;

        Type value = rawValue.value<Type>();

        if (validationStrategy.check(value))
            return value;

        return defaultValue;
    }

    bool save(Storage & settings, const QString & name, const Type& value)
    {
        if (validationStrategy.check(value))
        {
            settings.setValue(name, value);
            return true;
        }

        return false;
    }
private:
    ValidationStrategy validationStrategy;
};

#endif // DEFAULTPARAMETERSTRATEGY_H
