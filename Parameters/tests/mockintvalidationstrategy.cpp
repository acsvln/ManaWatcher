#include "mockintvalidationstrategy.h"

namespace auxiliary
{

MockIntValidationStrategy::MockIntValidationStrategy(MockIntValidationStrategy::Info &info,
                                                     bool checkResult):
    info(info),
    checkResult(checkResult)
{

}

MockIntValidationStrategy::MockIntValidationStrategy(MockIntValidationStrategy &&that):
    info(that.info),
    checkResult(that.checkResult)
{

}

bool MockIntValidationStrategy::check(const int & value) const
{
    this->info.method = Method::Check;
    this->info.value = value;
    return checkResult;
}

} //namespace auxiliary
