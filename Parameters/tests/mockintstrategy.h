#ifndef MOCKINTSTRATEGY_H
#define MOCKINTSTRATEGY_H

#include <QObject>
#include <QSettings>
#include <QString>

namespace auxiliary
{

class MockIntStrategy : public QObject
{
    Q_OBJECT
public:
    enum class Method {None, Load, Save};
    Q_ENUM(Method);

    struct Info
    {
        QSettings * settings = nullptr;
        QString name;
        int value = -1;
        Method method = Method::None;
    };

    MockIntStrategy(Info & info, int loadResult, bool saveResult);
    MockIntStrategy(MockIntStrategy&& that);

    int load(QSettings & settings, const QString & name, const int & defaultValue) const;
    bool save(QSettings & settings, const QString & name, const int & value);

private:
    Info & info;
    const int loadResult = 0;
    const bool saveResult = false;
};

} //namespace auxiliary

#endif // MOCKINTSTRATEGY_H
