#include "fileexistsvalidationstrategy.h"

#include <QFile>

bool FileExistsValidationStrategy::check(const QString &fileName) const
{
    return fileName.isEmpty() || QFile::exists(fileName);
}
