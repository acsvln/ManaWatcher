#ifndef PARAMETER_H
#define PARAMETER_H

#include <QSettings>
#include <QString>
#include <QVariant>

#include "defaultparameterstrategy.h"

template<typename Type,
         typename Strategy = DefaultParameterStrategy<Type>,
         typename Storage = QSettings>
class Parameter
{
public:
    Parameter(Storage & settings,
              const QString & name,
              const Type & defaultValue = Type(),
              Strategy && strategy = Strategy()):
        settings(settings),
        name(name),
        defaultValue(defaultValue),
        strategy(std::move(strategy))
    {

    }

    Type operator()() const
    {
        return this->strategy.load(this->settings, this->name, this->defaultValue);
    }

    operator Type() const
    {
        return this->operator ()();
    }

    bool operator()(const Type & value)
    {
        return this->strategy.save(this->settings, this->name, value);
    }

private:
    Storage & settings;
    const QString name;
    const Type defaultValue;
    Strategy strategy;
};

#endif // PARAMETER_H
