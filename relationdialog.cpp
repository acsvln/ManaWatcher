#include "relationdialog.h"
#include "ui_relationdialog.h"

RelationDialog::RelationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RelationDialog)
{
    Q_CHECK_PTR(this->ui);

    this->ui->setupUi(this);

    currentModel = new QStringListModel(this);
    this->ui->listView->setModel(currentModel);
    this->ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

RelationDialog::~RelationDialog()
{
    Q_CHECK_PTR(this->ui);

    delete this->ui;
}

void RelationDialog::setEnemies(PlayerList players)
{
    this->enemiesPlayers = players;
}

void RelationDialog::setFriends(PlayerList players)
{
    this->friendsPlayers = players;
}

void RelationDialog::showEvent(QShowEvent *event)
{
    Q_CHECK_PTR(this->ui);
    Q_UNUSED(event);

    updateModel(this->ui->comboBox->currentIndex());
}

void RelationDialog::updateModel(int index)
{
    Q_CHECK_PTR(this->currentModel);

    if (0 == index)
        this->currentModel->setStringList(this->friendsPlayers);
    else if (1 == index)
        this->currentModel->setStringList(this->enemiesPlayers);
    else
        Q_UNREACHABLE();
}

void RelationDialog::on_comboBox_currentIndexChanged(int index)
{
    updateModel(index);
}
