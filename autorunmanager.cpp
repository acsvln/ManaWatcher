#include "autorunmanager.h"

#include "definitions.h"

#include <QCoreApplication>
#include <QDir>

#ifdef Q_OS_LINUX
#include <QFile>
#include <QStandardPaths>
#include <QTextStream>
#endif

#ifdef Q_OS_WIN32
#include <QSettings>
const char * windowsAutorunRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
#endif

AutorunManager::AutorunManager(QObject *parent) : QObject(parent) {}

bool AutorunManager::isAutorunExists()
{
#ifdef Q_OS_LINUX
    QString autorunFolder = getAutorunFolder();

    return QFile::exists(autorunFolder + QLatin1String(autorunFileName));
#endif

#ifdef Q_OS_WIN32
    QSettings settings(
        windowsAutorunRegPath,
        QSettings::NativeFormat
    );

    return settings.childGroups().contains(applicationName, Qt::CaseInsensitive);
#endif
}

void AutorunManager::setup()
{
#ifdef Q_OS_WIN32
    QSettings settings(windowsAutorunRegPath, QSettings::NativeFormat);
    settings.setValue(applicationName, QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
    settings.sync();
#endif

#ifdef Q_OS_LINUX
    aquireAutorunFile([this](QFile& autorunFile){
        // Далее проверяем наличие самого файла автозапуска
        if(!autorunFile.exists()){

            /* Далее открываем файл и записываем в него необходимые данные
             * с указанием пути к исполняемому файлу, с помощью QCoreApplication::applicationFilePath()
             * */
            if(autorunFile.open(QFile::WriteOnly)){
                QString autorunContent = this->getAutorunFileContents();

                QTextStream outStream(&autorunFile);
                outStream << autorunContent;
                // Устанавливаем права доступа, в том числе и на исполнение файла, иначе автозапуск не сработает
                autorunFile.setPermissions(QFileDevice::ExeUser|QFileDevice::ExeOwner|QFileDevice::ExeOther|QFileDevice::ExeGroup|
                                       QFileDevice::WriteUser|QFileDevice::ReadUser);
                autorunFile.close();
            }
        }
    });

#endif
}

void AutorunManager::unsetup()
{
#ifdef Q_OS_WIN32
    QSettings settings(windowsAutorunRegPath, QSettings::NativeFormat);
    settings.remove(applicationName);
#endif

#ifdef Q_OS_LINUX
    aquireAutorunFile([](QFile& autorunFile){
        // Удаляем файл автозапуска
        if(autorunFile.exists())
            autorunFile.remove();
    });
#endif
}
#ifdef Q_OS_LINUX

void AutorunManager::aquireAutorunFile(auto onAquire)
{
    // Путь к папке автозапуска
    QString autostartPath = getAutorunFolder();
    /* Проверяем, существует ли директория, в которой будет храниться файл автозапуска.
    * А то мало ли... пользователь удалил...
    * */
    QDir autorunDir(autostartPath);
    if(!autorunDir.exists()){
        // Если не существует, то создаём
        autorunDir.mkpath(autostartPath);
    }

    QFile autorunFile(autostartPath + QLatin1String(autorunFileName));

    onAquire(autorunFile);
}

QString AutorunManager::getAutorunFolder()
{
    return QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + QLatin1String("/autostart");
}

QString AutorunManager::getAutorunFileContents()
{
    QString fileName(":/template/autorun"); // TODO

    QFile file(fileName);

    if(!file.open(QIODevice::ReadOnly))
        Q_UNREACHABLE();

    return QString(file.readAll())
           .arg(QCoreApplication::applicationFilePath())
           .arg(QCoreApplication::applicationDirPath())
           .arg(QCoreApplication::applicationName());
}

#endif
