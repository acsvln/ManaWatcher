#ifndef BASICSETTINGSINMEMORY_H
#define BASICSETTINGSINMEMORY_H

#include <basicsettings.h>
#include <inmemorystorage.h>

class BasicSettingsInMemory :
        private InMemoryStorage,
        public BasicSettings<InMemoryStorage>
{
    using Base = BasicSettings<InMemoryStorage>;
public:
    BasicSettingsInMemory();
};

#endif // BASICSETTINGSINMEMORY_H
