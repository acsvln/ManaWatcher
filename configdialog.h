#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>
#include <basicsettingsinmemory.h>

namespace Ui {
    class ConfigDialog;
}

class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConfigDialog(BasicSettingsInMemory & settings, QWidget *parent = 0);
    ~ConfigDialog();

public slots:
    void accept() override;

protected:
    void closeEvent(QCloseEvent * event) override;

private slots:
    void on_buttonLocateConfig_clicked();

private:
    Ui::ConfigDialog *ui;

    BasicSettingsInMemory& settings;

    bool setPeriod(int period);
    void initPeriods();

    bool save();
    bool load();
};

#endif // CONFIGDIALOG_H
