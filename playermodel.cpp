#include "playermodel.h"

#include <QBrush>

PlayerModel::PlayerModel(QObject *parent) :
    QAbstractListModel(parent),
    dataModel(new QStringListModel(this))
{

}

void PlayerModel::setDataSnapshot(PlayerList data)
{
    Q_CHECK_PTR(this->dataModel);

    beginResetModel();

    this->dataModel->setStringList(data);

    endResetModel();
}

void PlayerModel::setFriends(PlayerList data)
{
    this->friends= data.toSet();
}

void PlayerModel::setEnemies(PlayerList data)
{
    this->enemies = data.toSet();
}

int PlayerModel::rowCount(const QModelIndex &parent) const
{
    Q_ASSERT(!parent.isValid());

    return this->dataModel->rowCount(QModelIndex());
}

QVariant PlayerModel::data(const QModelIndex &index, int role) const
{
    Q_CHECK_PTR(this->dataModel);

    Q_ASSERT(index.column() == 0);

    if (role == Qt::BackgroundRole)
    {
        QString player = index.data().toString();

        if (this->friends.contains(player))
            return QBrush(Qt::darkGreen);

        if (this->enemies.contains(player))
           return QBrush(Qt::darkRed);
    }

    if (role == Qt::TextColorRole)
    {
        QString player = index.data().toString();

        if (this->friends.contains(player) || this->enemies.contains(player))
            return QBrush(Qt::white);
    }

    return this->dataModel->index(index.row(), 0).data(role);
}
