#include "playerwatcher.h"

PlayerWatcher::PlayerWatcher(QObject *parent) : QObject(parent)
{

}

void PlayerWatcher::execute(PlayerList data)
{  
    if (!this->all.isEmpty())
    {
        PlayerCollection newPlayersBuffer=data.toSet();
        PlayerCollection oldPlayersBuffer = this->all;

        PlayerCollection& loggedOut = oldPlayersBuffer.subtract(newPlayersBuffer);
        PlayerCollection& loggedIn = newPlayersBuffer.subtract(this->all);

        EventDataType event;

        auto enumerateCollection = [&event, this] (auto collection, EventType type) {
            QStringList friendsBuffer, enemiesBuffer;

            foreach (QString player, collection)
            {
                if (this->friends.contains(player))
                    friendsBuffer.append(player);
                else if (this->enemies.contains(player))
                    enemiesBuffer.append(player);
            }

            if (!friendsBuffer.isEmpty())
                event.push_back(EventDataType::value_type(type, RelationType::Friend, friendsBuffer));

            if (!enemiesBuffer.isEmpty())
                event.push_back(EventDataType::value_type(type, RelationType::Enemy, friendsBuffer));
        };

        enumerateCollection(loggedOut, EventType::LogoutEvent);
        enumerateCollection(loggedIn, EventType::LoginEvent);

        if (!event.isEmpty())
            emit eventFired(event);
    }

    this->all = data.toSet();
}

void PlayerWatcher::setFriends(PlayerList data)
{
    this->friends=data.toSet();
}

void PlayerWatcher::setEnemies(PlayerList data)
{
    this->enemies=data.toSet();
}

