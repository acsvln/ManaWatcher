#ifndef DEFINITIONS
#define DEFINITIONS

#include <QStringList>

extern const char * applicationName;
extern const char * applicationVersion;

extern const char * trayToolTip;

extern const char * defaultConfigFileName;
extern const char * defaultLookupURL;
extern const int defaultPeriod;

#ifdef Q_OS_LINUX
extern const char * autorunFileName;
#endif

typedef QStringList PlayerList;

#endif // DEFINITIONS

