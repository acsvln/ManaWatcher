#include "playersortmodel.h"

PlayerSortModel::PlayerSortModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

void PlayerSortModel::setFilterLimits(PlayerSortModel::FilterLimits limit)
{
    if (this->limit == limit)
        return;

    beginResetModel();

    this->limit = limit;

    endResetModel();
}

PlayerSortModel::FilterLimits PlayerSortModel::filterLimits()
{
    return limit;
}

void PlayerSortModel::setFriends(PlayerList data)
{
    this->friends = data.toSet();
    invalidate();
}

void PlayerSortModel::setEnemies(PlayerList data)
{
    this->enemies = data.toSet();
    invalidate();
}

bool PlayerSortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    bool result = QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent);

    if (result)
    {
        Q_CHECK_PTR(sourceModel());

        QModelIndex sourceIndex = sourceModel()->index(source_row, filterKeyColumn(), source_parent);

        Q_ASSERT(sourceIndex.isValid());

        QString player = sourceIndex.data().toString();

        Q_ASSERT(!player.isEmpty());

        bool isFriend = friends.contains(player), isEnemy = enemies.contains(player);

        if (!limit.friends && isFriend)
            return false;

        if (!limit.enemies && isEnemy)
            return false;

        if (!limit.others && !isFriend && !isEnemy)
            return false;
    }

    return result;
}

bool PlayerSortModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    if (RelationSortRole == this->sortRole())
    {
        Q_ASSERT(source_left.isValid());
        Q_ASSERT(source_right.isValid());

        QString firstPlayer, secondPlayer;

        firstPlayer = source_left.data().toString();
        secondPlayer = source_right.data().toString();

        auto check = [firstPlayer, secondPlayer](QSet<QString> container)
        {
            unsigned result = 0x00;

            if (container.contains(firstPlayer))
                result |= 0x01;

            if (container.contains(secondPlayer))
                result |= 0x02;

            return result;
        };

        unsigned res1 = check(this->friends);
        unsigned res2 = check(this->enemies);

        if ((res1 == 0x03) || (res2 == 0x03) || (0x00 == (res2 | res1)))
            return QString::compare(firstPlayer, secondPlayer, Qt::CaseInsensitive) < 0;

        if (Qt::AscendingOrder == this->sortOrder())
            return ((res1 & 0x01) && (res2 & 0x02)) || ((res1 & 0x01) && (res2 == 0x00)) || ((res2 & 0x01) && (res1 == 0x00));
         else if (Qt::DescendingOrder == this->sortOrder())
            return ((res2 & 0x01) && (res1 & 0x02)) || ((res2 & 0x01) && (res1 == 0x00)) || ((res1 & 0x01) && (res2 == 0x00));

        Q_UNREACHABLE();
    }

    return QSortFilterProxyModel::lessThan(source_left, source_right);
}
