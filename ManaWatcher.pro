#-------------------------------------------------
#
# Project created by QtCreator 2017-10-22T18:19:26
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

TARGET = ManaWatcher
TEMPLATE = app

include(SingleApplication/singleapplication.pri)
include(Parameters/parameters.pri)

DEFINES += QAPPLICATION_CLASS=QApplication

SOURCES += main.cpp\
        mainwindow.cpp \
    relationresolver.cpp \
    playerwatcher.cpp \
    definitions.cpp \
    aboutdialog.cpp \
    configdialog.cpp \
    relationdialog.cpp \
    autorunmanager.cpp \
    playersortmodel.cpp \
    playersupplier.cpp \
    playermodel.cpp \
    settings.cpp \
    basicsettingsinmemory.cpp

HEADERS  += mainwindow.h \
    definitions.h \
    playerwatcher.h \
    aboutdialog.h \
    configdialog.h \
    relationdialog.h \
    autorunmanager.h \
    playersortmodel.h \
    playersupplier.h \
    playermodel.h \
    relationresolver.h \
    settings.h \
    basicsettings.h \
    basicsettingsinmemory.h

FORMS    += mainwindow.ui \
    aboutdialog.ui \
    configdialog.ui \
    relationdialog.ui

RESOURCES += \
    resources.qrc

DISTFILES += \
    README.md \
    LICENSE \
    CHANGELOG.md
