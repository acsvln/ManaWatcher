#include "settings.h"
#include "definitions.h"
#include "playersortmodel.h"

Settings::Settings(QSettings * settings, QObject * parent) :
    QObject(parent),
    Base(*settings),
    autoRefresh(*settings, "AutoRefresh", false),
    mainWindowGeometry(*settings, "mainWindowGeometry"),
    mainWindowState(*settings, "mainWindowState"),
    includeEnemies(*settings, "FilterIncludeEnemies", true),
    includeFriends(*settings, "FilterIncludeFriends", true),
    includeOthers(*settings, "FilterIncludeOthers", true),
    sortMode(*settings, "SortMode", Qt::InitialSortOrderRole,
             KeySetParameterStrategy<int>{
                {Qt::DisplayRole, "aplhabetic"},
                {PlayerSortModel::RelationSortRole, "relation"},
                {Qt::InitialSortOrderRole, "default"}}),
    sortOrder(*settings, "SortOrder", Qt::AscendingOrder),
    firstClose(*settings, "FirstClose", true),
    settings(settings)
{
    this->settings->setParent(this);
}
