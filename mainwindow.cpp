﻿#include "aboutdialog.h"
#include "configdialog.h"
#include "mainwindow.h"
#include "relationdialog.h"

#include "boost/date_time/posix_time/posix_time.hpp"

#include "ui_mainwindow.h"

#include <QCloseEvent>
#include <QStyle>
#include <QDesktopWidget>

MainWindow::MainWindow(QString configPath, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    Q_CHECK_PTR(this->ui);

    this->ui->setupUi(this);

    this->settings = new Settings(
                new QSettings(configPath, QSettings::IniFormat, this),
                this
                );

    initBusinessObjects();
    initViewMenu();
    initTray();
    initTimer();
    initMisc();

    restoreWindow();
    retoreSort();

    Q_CHECK_PTR(this->playerSupplier);
    this->playerSupplier->request();
}

MainWindow::~MainWindow()
{
    Q_CHECK_PTR(this->ui);

    keepSort();

    delete this->ui;
}

void MainWindow::keepSort()
{
    Q_CHECK_PTR(this->playerSortModel);
    Q_CHECK_PTR(this->settings);

    PlayerSortModel::FilterLimits filterLimits = playerSortModel->filterLimits();

    this->settings->includeEnemies(filterLimits.enemies);
    this->settings->includeFriends(filterLimits.friends);
    this->settings->includeOthers(filterLimits.others);

    int sortRole = this->playerSortModel->sortRole();

    this->settings->sortMode(sortRole);

    this->settings->sortOrder(currentSortOrder());
}

void MainWindow::initMisc()
{
    Q_CHECK_PTR(this->ui);
    Q_CHECK_PTR(this->settings);

    this->ui->actionAutoRefresh->setChecked(this->settings->autoRefresh());
    this->ui->actionAutorun->setChecked(this->autorunManager->isAutorunExists());

    connect(this->ui->actionRelationRead, SIGNAL(triggered()), this, SLOT(readRelations()));
    connect(this->ui->actionQuit, SIGNAL(triggered(bool)), this, SLOT(close()));
}

void MainWindow::restoreWindow()
{
    Q_CHECK_PTR(this->settings);

    setWindowFlags(Qt::Window | Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);

    QByteArray mainWindowGeometry = this->settings->mainWindowGeometry();
    if (mainWindowGeometry.isEmpty())
        setGeometry(
                QStyle::alignedRect(
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    size(),
                    qApp->desktop()->availableGeometry()
                )
         );
    else
        restoreGeometry(mainWindowGeometry);

    QByteArray mainWindowState = this->settings->mainWindowState();
    if (!mainWindowState.isEmpty())
        restoreState(mainWindowState);
}

void MainWindow::retoreSort()
{
    Q_CHECK_PTR(this->settings);
    Q_CHECK_PTR(this->playerSortModel);
    Q_CHECK_PTR(this->ui);

    bool enemies = true, friends = true, other = true;

    enemies = this->settings->includeEnemies();
    friends = this->settings->includeFriends();
    other = this->settings->includeOthers();

    PlayerSortModel::FilterLimits limits;

    limits.friends = friends;
    limits.enemies = enemies;
    limits.others = other;

    this->playerSortModel->setFilterLimits(limits);

    int sortRole = this->settings->sortMode();

    this->playerSortModel->setSortRole(sortRole);

    this->ui->actionNoSort->setChecked(Qt::InitialSortOrderRole == sortRole);
    this->ui->actionSortByName->setChecked(Qt::DisplayRole == sortRole);
    this->ui->actionSortByRelation->setChecked(PlayerSortModel::RelationSortRole == sortRole);

    Qt::SortOrder sortOrder = this->settings->sortOrder();

    this->ui->actionSortAscending->setChecked(Qt::AscendingOrder == sortOrder);
    this->ui->actionSortDescending->setChecked(Qt::DescendingOrder == sortOrder);

    if ( Qt::InitialSortOrderRole != sortRole )
        this->playerSortModel->sort(0, sortOrder);

    this->ui->actionFilterIncludeFriends->setChecked(friends);
    this->ui->actionFilterIncludeEnemies->setChecked(enemies);
    this->ui->actionFilterIncludeOthers->setChecked(other);
}

void MainWindow::initBusinessObjects()
{
    Q_CHECK_PTR(this->settings);
    Q_CHECK_PTR(this->ui);

    this->playerSupplier = new PlayerSupplier(this->settings->url(), this);

    this->playerModel = new PlayerModel(this);
    this->playerSortModel = new PlayerSortModel(this);

    this->relationResolver = new RelationResolver(this);
    this->playerWatcher = new PlayerWatcher(this);

    this->autorunManager = new AutorunManager(this);

    connect(this->relationResolver, SIGNAL(enemiesResolved(PlayerList)), this->playerModel, SLOT(setEnemies(PlayerList)));
    connect(this->relationResolver, SIGNAL(friendsResolved(PlayerList)), this->playerModel, SLOT(setFriends(PlayerList)));

    connect(this->relationResolver, SIGNAL(enemiesResolved(PlayerList)), this->playerWatcher, SLOT(setEnemies(PlayerList)));
    connect(this->relationResolver, SIGNAL(friendsResolved(PlayerList)), this->playerWatcher, SLOT(setFriends(PlayerList)));

    connect(this->relationResolver, SIGNAL(enemiesResolved(PlayerList)), this->playerSortModel, SLOT(setEnemies(PlayerList)));
    connect(this->relationResolver, SIGNAL(friendsResolved(PlayerList)), this->playerSortModel, SLOT(setFriends(PlayerList)));

    connect(this->playerSupplier, SIGNAL(suppply(PlayerList)), this->playerModel, SLOT(setDataSnapshot(PlayerList)));
    connect(this->playerSupplier, SIGNAL(suppply(PlayerList)), this->playerWatcher, SLOT(execute(PlayerList)));

    connect(this->playerWatcher, SIGNAL(eventFired(PlayerWatcher::EventDataType)), this, SLOT(popupMessage(PlayerWatcher::EventDataType)));

    connect(this->ui->actionRefreshNow, SIGNAL(triggered()), this->playerSupplier, SLOT(request()));

    this->playerSortModel->setSourceModel(this->playerModel);

    if (!this->settings->config().isEmpty())
    {
        this->relationResolver->setPath(this->settings->config());
        this->relationResolver->read();
    }

    this->ui->listView->setModel(playerSortModel);
}

void MainWindow::keepWindow()
{
    Q_CHECK_PTR(this->settings);

    if (!this->isVisible())
        return;

    this->settings->mainWindowGeometry(saveGeometry());
    this->settings->mainWindowState(saveState());
}

void MainWindow::initTimer()
{
    Q_CHECK_PTR(this->settings);

    this->timer = new QTimer(this);

    connect(this->timer, SIGNAL(timeout()), this->playerSupplier, SLOT(request()));

    using namespace boost::posix_time;

    time_duration td = minutes(this->settings->period());

    this->timer->setInterval(td.total_milliseconds());

    if (this->settings->autoRefresh())
        this->timer->start();
}

void MainWindow::initViewMenu()
{
    Q_CHECK_PTR(this->ui);

    auto initSortOrder = [this]
    {
        QActionGroup* sortGroup = new QActionGroup( this );

        this->ui->actionSortAscending->setCheckable(true);
        this->ui->actionSortDescending->setCheckable(true);

        this->ui->actionSortAscending->setActionGroup(sortGroup);
        this->ui->actionSortDescending->setActionGroup(sortGroup);
    };

    auto initSortModes = [this]
    {
        QActionGroup* viewGroup = new QActionGroup( this );

        this->ui->actionSortByName->setCheckable(true);
        this->ui->actionNoSort->setCheckable(true);
        this->ui->actionSortByRelation->setCheckable(true);

        this->ui->actionSortByName->setActionGroup(viewGroup);
        this->ui->actionNoSort->setActionGroup(viewGroup);
        this->ui->actionSortByRelation->setActionGroup(viewGroup);
    };

    auto initAction = [this]
    {
        connect(this->ui->actionFilterIncludeFriends, SIGNAL(triggered(bool)), this, SLOT(updateFilter()));
        connect(this->ui->actionFilterIncludeEnemies, SIGNAL(triggered(bool)), this, SLOT(updateFilter()));
        connect(this->ui->actionFilterIncludeOthers, SIGNAL(triggered(bool)), this, SLOT(updateFilter()));

        connect(this->ui->actionSortAscending, SIGNAL(triggered(bool)), this, SLOT(updateSort()));
        connect(this->ui->actionSortDescending, SIGNAL(triggered(bool)), this, SLOT(updateSort()));
    };

    initSortModes();
    initSortOrder();
    initAction();
}

void MainWindow::initTray()
{
    Q_CHECK_PTR(this->ui);

    this->trayIcon = new QSystemTrayIcon(this);

    QIcon icon(":/img/logo");

    this->trayIcon->setIcon(icon);
    this->trayIcon->setToolTip(trayToolTip);

    QMenu * menu = new QMenu(this);

    connect(ui->actionQuit, SIGNAL(triggered(bool)), trayIcon, SLOT(hide()));

    menu->addAction(ui->actionRefreshNow);
    menu->addAction(ui->actionAutoRefresh);
    menu->addAction(ui->actionShowMainWindow);
    menu->addAction(ui->actionQuit);

    connect(this->trayIcon, &QSystemTrayIcon::activated, [this](QSystemTrayIcon::ActivationReason reason){
        if (QSystemTrayIcon::Trigger == reason)
        {
            if (!this->isVisible())
                this->show();

            this->raise();
            this->activateWindow();
            this->showNormal();
        }
    });

    this->trayIcon->setContextMenu(menu);
    this->trayIcon->show();
}

Qt::SortOrder MainWindow::currentSortOrder()
{
    Q_CHECK_PTR(this->ui);

    if (ui->actionSortAscending->isChecked())
        return Qt::AscendingOrder;

    if (ui->actionSortDescending->isChecked())
        return Qt::DescendingOrder;

    Q_UNREACHABLE();
}

void MainWindow::updateFilter()
{
    Q_CHECK_PTR(this->playerSortModel);

    PlayerSortModel::FilterLimits limits = this->playerSortModel->filterLimits();

    limits.friends = this->ui->actionFilterIncludeFriends->isChecked();
    limits.enemies = this->ui->actionFilterIncludeEnemies->isChecked();
    limits.others = this->ui->actionFilterIncludeOthers->isChecked();

    this->playerSortModel->setFilterLimits(limits);   
}

void MainWindow::updateSort()
{
    Q_CHECK_PTR(this->playerSortModel);

    this->playerSortModel->sort(0, currentSortOrder());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_CHECK_PTR(this->trayIcon);
    Q_CHECK_PTR(this->settings);

    if (!this->trayIcon->isVisible())
        keepWindow();
    else
    {
        if (this->settings->firstClose())
        {
            this->trayIcon->showMessage("ManaWatcher", "The program will keep running in the "
                                                 "system tray. To terminate the program, "
                                                 "choose \"Quit\" in the context menu "
                                                 "of the system tray entry.");

            this->settings->firstClose(false);
        }

        keepWindow();
        hide();
        event->ignore();
    }
}

void MainWindow::popupMessage(PlayerWatcher::EventDataType data)
{
    Q_CHECK_PTR(this->trayIcon);

    QString title = "TheManaWorld";
    QString message;
    QTextStream textStream(&message, QIODevice::WriteOnly | QIODevice::Text);

    foreach (PlayerWatcher::EventDataType::value_type item, data)
    {
        PlayerWatcher::RelationType relationType = std::get<1>(item);
        PlayerWatcher::EventType eventType = std::get<0>(item);
        QStringList players = std::get<2>(item);

        bool isMany = players.size() > 1;
        bool isEnemy = (PlayerWatcher::RelationType::Enemy == relationType), isFriend = (PlayerWatcher::RelationType::Friend == relationType);

        QString relationString = (isFriend && isMany) ? "friends" : (isFriend && !isMany) ? "friend" : (isEnemy && isMany) ? "enemies" : "enemy";
        QString playersString = (players.count() > 1) ? players.join(',') : players.at(0);
        QString eventString = (PlayerWatcher::EventType::LoginEvent == eventType) ? "in" : "out";

        textStream << "Your" << " "
                   << relationString << " "
                   << playersString << " "
                   << "logged" << " "
                   << eventString << "\n";
    }

    this->trayIcon->showMessage(title, message.trimmed(), QSystemTrayIcon::Information);
}

void MainWindow::on_actionAboutDialog_triggered()
{
    AboutDialog dialog;
    centerDialog(dialog);
    dialog.exec();
}

void MainWindow::centerDialog(QDialog & dialog)
{
    dialog.move(x() + (width() - dialog.width()) / 2,
                 y() + (height() - dialog.height()) / 2);
}

bool MainWindow::readRelations()
{
    Q_CHECK_PTR(this->settings);
    Q_CHECK_PTR(this->relationResolver);

    if (this->settings->config().isEmpty())
    {
        qWarning() << "No config file set!";
        return false;
    }
    else
    {
        this->relationResolver->read();
        return true;
    }
}

void MainWindow::on_actionRelationDialog_triggered()
{
    Q_CHECK_PTR(this->relationResolver);

    RelationDialog dialog;

    centerDialog(dialog);

    connect(this->relationResolver, SIGNAL(enemiesResolved(PlayerList)), &dialog, SLOT(setEnemies(PlayerList)));
    connect(this->relationResolver, SIGNAL(friendsResolved(PlayerList)), &dialog, SLOT(setFriends(PlayerList)));

    if (readRelations())
        dialog.exec();
}

void MainWindow::on_actionConfigurationDialog_triggered()
{
    Q_CHECK_PTR(this->settings);
    Q_CHECK_PTR(this->relationResolver);
    Q_CHECK_PTR(this->playerSupplier);
    Q_CHECK_PTR(this->timer);

    BasicSettingsInMemory editableSettings;
    assign(editableSettings, *this->settings);
    ConfigDialog dialog(editableSettings);

    centerDialog(dialog);

    if (QDialog::Accepted == dialog.exec())
    {
        assign(*this->settings, editableSettings);

        this->relationResolver->setPath(this->settings->config());
        if (!this->settings->config().isEmpty())
            this->relationResolver->read();

        this->playerSupplier->setUrl(this->settings->url());
        this->playerSupplier->request();

        using namespace boost::posix_time;

        time_duration td = minutes(this->settings->period());

        this->timer->setInterval(td.total_milliseconds());
    }
}

void MainWindow::on_actionAutorun_triggered(bool checked)
{
    Q_CHECK_PTR(this->autorunManager);

    if (checked)
        this->autorunManager->setup();
    else
        this->autorunManager->unsetup();
}

void MainWindow::on_actionAutoRefresh_triggered(bool checked)
{
    Q_CHECK_PTR(this->settings);
    Q_CHECK_PTR(this->timer);

    this->settings->autoRefresh(checked);

    if (checked)
        this->timer->start();
    else
        this->timer->stop();
}

void MainWindow::on_actionNoSort_triggered()
{
    Q_CHECK_PTR(this->playerSortModel);

    this->playerSortModel->setSortRole(Qt::InitialSortOrderRole);
    this->playerSortModel->invalidate();
//  Does not work:
//    this->playerSortModel->sort(0, currentSortOrder());
}

void MainWindow::on_actionSortByRelation_triggered()
{
    Q_CHECK_PTR(this->playerSortModel);

    this->playerSortModel->setSortRole(PlayerSortModel::RelationSortRole);
    this->playerSortModel->sort(0, currentSortOrder());
}

void MainWindow::on_actionSortByName_triggered()
{
    Q_CHECK_PTR(this->playerSortModel);

    this->playerSortModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    this->playerSortModel->setSortRole(Qt::DisplayRole);
    this->playerSortModel->sort(0, currentSortOrder());
}
