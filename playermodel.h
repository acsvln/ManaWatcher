#ifndef ONLINEPLAYERSMODEL_H
#define ONLINEPLAYERSMODEL_H

#include "definitions.h"

#include <QAbstractListModel>
#include <QSet>
#include <QStringList>
#include <QStringListModel>

class PlayerModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit PlayerModel(QObject *parent = 0);

public slots:
    void setDataSnapshot(PlayerList data);
    void setFriends(PlayerList data);
    void setEnemies(PlayerList data);

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

private:
    QStringListModel * dataModel = nullptr;

    QSet<QString> friends;
    QSet<QString> enemies;
};

#endif // ONLINEPLAYERSMODEL_H
