#ifndef AUTORUNMANAGER_H
#define AUTORUNMANAGER_H

#include <QObject>

class AutorunManager : public QObject
{
    Q_OBJECT
public:
    explicit AutorunManager(QObject *parent = nullptr);

    bool isAutorunExists();
    void setup();
    void unsetup();

#ifdef Q_OS_LINUX
private:
    void aquireAutorunFile(auto onAquire);
    QString getAutorunFolder();
    QString getAutorunFileContents();
#endif
};

#endif // AUTORUNMANAGER_H
