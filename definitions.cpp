#include "definitions.h"

const char * applicationName = "ManaWatcher";
const char * applicationVersion = "0.2.0";

const char * trayToolTip = "ManaWatcher buddy";

const char * defaultConfigFileName = "ManaWatcher.ini";
const char * defaultLookupURL = "https://updates.themanaworld.org/updates/online.txt";
const int defaultPeriod = 5;

#ifdef Q_OS_LINUX
const char * autorunFileName = "/ManaWatcher.desktop";
#endif
