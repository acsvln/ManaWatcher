#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QTimer>

#include "autorunmanager.h"
#include "playermodel.h"
#include "playersortmodel.h"
#include "playerwatcher.h"
#include "relationresolver.h"
#include "playersupplier.h"
#include "settings.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString configPath, QWidget *parent = 0);
    ~MainWindow();

// QWidget interface
protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void popupMessage(PlayerWatcher::EventDataType data);

    void on_actionAboutDialog_triggered();
    void on_actionRelationDialog_triggered();
    void on_actionConfigurationDialog_triggered();
    void on_actionAutorun_triggered(bool checked);
    void on_actionAutoRefresh_triggered(bool checked);
    void on_actionNoSort_triggered();
    void on_actionSortByRelation_triggered();
    void on_actionSortByName_triggered();

private:
    void initViewMenu();
    void initTray();
    void initBusinessObjects();
    void initTimer();
    void initMisc();

    void restoreWindow();
    void keepWindow();

    void retoreSort();
    void keepSort();

    void centerDialog(QDialog & dialog);
    Qt::SortOrder currentSortOrder();

private slots:
    void updateFilter();
    void updateSort();
    bool readRelations();
private:
    AutorunManager * autorunManager = nullptr;
    PlayerWatcher * playerWatcher = nullptr;
    RelationResolver * relationResolver = nullptr;
    PlayerSupplier * playerSupplier = nullptr;
    PlayerModel * playerModel = nullptr;
    PlayerSortModel * playerSortModel = nullptr;

    QSystemTrayIcon * trayIcon = nullptr;
    QTimer * timer = nullptr;

    Settings * settings = nullptr;

    Ui::MainWindow *ui = nullptr;
};

#endif // MAINWINDOW_H
