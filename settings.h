#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <parameter.h>
#include <qenumparameterstrategy.h>
#include <keysetparameterstrategy.h>
#include <basicsettings.h>

class Settings : public QObject, public BasicSettings<QSettings>
{
    Q_OBJECT
    using Base = BasicSettings<QSettings>;
public:
    /* The "settings" pointer mustn't be null. After creating Config become
     * the owner of the "settings" object.
     */
    explicit Settings(QSettings * settings, QObject * parent = nullptr);

    Parameter<bool> autoRefresh;

    Parameter<QByteArray> mainWindowGeometry;
    Parameter<QByteArray> mainWindowState;

    Parameter<bool> includeEnemies;
    Parameter<bool> includeFriends;
    Parameter<bool> includeOthers;

    Parameter<int, KeySetParameterStrategy<int>> sortMode;
    Parameter<Qt::SortOrder, QEnumParameterStrategy<Qt::SortOrder>> sortOrder;

    Parameter<bool> firstClose;
private:
    QSettings * settings;
};

#endif // SETTINGS_H
