#include "playersupplier.h"

#include <QStringList>

#include <string>
#include <stdexcept>
#include <regex>

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted/std_tuple.hpp>

PlayerResolver::PlayerResolver(QObject *parent) : QObject(parent)
{
    this->networkManager = new QNetworkAccessManager();
    // Подключаем networkManager к обработчику ответа
    connect(this->networkManager, &QNetworkAccessManager::finished, this, &PlayerResolver::onResult);
}

void PlayerResolver::setUrl(QString url)
{
    this->url.setUrl(url);
}

void PlayerResolver::execute()
{
   Q_CHECK_PTR(this->networkManager);

   // Получаем данные, а именно JSON файл с сайта по определённому url
   this->networkManager->get(QNetworkRequest(url));
}

void PlayerResolver::onResult(QNetworkReply *reply)
{
    Q_CHECK_PTR(reply);

    // Если ошибки отсутсвуют
    if(!reply->error()) {
        emit dataReceived(reply->readAll());
    }

    reply->deleteLater();
}

PlayerParser::PlayerParser(QObject *parent) : QObject(parent)
{

}

void PlayerParser::parseData(QByteArray data)
{
    auto parse_expression = [](std::string expression){
        namespace qi = boost::spirit::qi;
        namespace ns = boost::spirit::qi::ascii;

        typedef std::string 	str_t;
        typedef str_t server_name_t;
        typedef str_t date_t;
        typedef str_t::iterator str_it_t;
        typedef std::vector<std::string> list_t;
        typedef std::tuple<server_name_t, date_t, list_t, unsigned> result_t;

        str_it_t begin = std::begin(expression), end = std::end(expression);

        qi::rule<str_it_t, str_t()> date_rule = qi::int_ >> qi::lit("-") >> qi::int_ >> qi::lit("-")
                                                                >> qi::int_ >>  ns::space
                                                                >> qi::int_ >> qi::lit(":") >> qi::int_ >>  qi::lit(":") >> qi::int_;

        qi::rule<str_it_t, str_t()> date_as_string_rule = qi::omit[&date_rule] > *(!qi::lit(')') >> qi::char_);
        qi::rule<str_it_t, str_t()> server_name_rule = *(!qi::lit(" (") >> qi::char_("a-zA-Z0-9 "));
        qi::rule<str_it_t, list_t()> players_rule = +qi::char_("a-zA-Z0-9 -_|") % qi::eol;
        qi::rule<str_it_t, result_t()> main_rule = qi::lit("Online Players on ")
            >> server_name_rule >> qi::lit(" (") >> date_as_string_rule >> qi::lit("):")
            >> qi::omit[ qi::eol >>  qi::eol >> qi::lit("Name") >> *ns::space >> +qi::lit('-') >> qi::eol]
            >> players_rule >> qi::omit[qi::eol >> qi::eol] >> qi::int_ >> qi::lit(" users are online.") >> qi::omit[qi::eol];

        result_t result;

        bool success = qi::parse(
            begin, end,
            main_rule,
            result);

        if (!success || (begin != end))
            throw std::runtime_error ("Parse error");

        return result;
    };

    PlayerList result;

    try
    {
        auto parsed = parse_expression(QString(data).toStdString());
        auto player_list = std::get<2>(parsed);
        auto player_count = std::get<3>(parsed);

        if (player_list.size() != player_count)
            throw std::runtime_error("Invalid player count");

        for (auto name : player_list)
        {
            std::smatch cm;
            // ((?:\w|\s)+)\s\(GM\)
            std::regex e("((?:\\w|\\s)+)\\s\\(GM\\)");

            QString value;

            if (std::regex_search ( name, cm, e))
                value = QString::fromStdString(cm[1].str());
            else
                value = QString::fromStdString(name);

            value = value.trimmed();

            result.append(value);
        }
    }
    catch (std::runtime_error& e)
    {
        qCritical() << e.what();
    }

    // TODO: Check GM!
    emit dataParsed(result);
}

PlayerSupplier::PlayerSupplier(const QString & url, QObject *parent) : QObject(parent)
{
    this->resolver = new PlayerResolver(this);
    this->parser = new PlayerParser(this);

    this->setUrl(url);

    connect(this->resolver, SIGNAL(dataReceived(QByteArray)), this->parser, SLOT(parseData(QByteArray)));
    connect(this->parser, SIGNAL(dataParsed(PlayerList)), this, SIGNAL(suppply(PlayerList)));
}

void PlayerSupplier::request()
{
    Q_CHECK_PTR(this->resolver);

    this->resolver->execute();
}

void PlayerSupplier::setUrl(const QString &url)
{
    this->resolver->setUrl(url);
}
