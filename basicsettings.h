#ifndef BASICSETTINGS_H
#define BASICSETTINGS_H

#include "parameter.h"
#include "urlvalidationstrategy.h"
#include "fileexistsvalidationstrategy.h"
#include "definitions.h"

template<typename Storage>
class BasicSettings
{
public:
    explicit BasicSettings(Storage& storage):
        url(storage, "Url", defaultLookupURL),
        config(storage, "ManaConfig"),
        period(storage, "RefreshTime", defaultPeriod)
    {}

    template<typename Type, typename ValidationStrategy = NoneValidationStrategy<Type>>
    using DefaultParameterStrategy = ::DefaultParameterStrategy<Type, ValidationStrategy, Storage>;

    template<typename Type, typename Strategy = DefaultParameterStrategy<Type>>
    using Parameter = ::Parameter<Type, Strategy, Storage>;

    using UrlParameterStrategy = DefaultParameterStrategy<QString, UrlValidationStrategy>;
    using FileParameterStrategy = DefaultParameterStrategy<QString, FileExistsValidationStrategy>;

    Parameter<QString, UrlParameterStrategy> url;
    Parameter<QString, FileParameterStrategy> config;
    Parameter<int> period;

};

template<typename LeftStorage, typename RightStorage>
bool assign(BasicSettings<LeftStorage>& dst, const BasicSettings<RightStorage>& src)
{
    bool res = true;
    res = dst.url(src.url()) && res;
    res = dst.config(src.config()) && res;
    res = dst.period(src.period()) && res;
    return res;
}


#endif // BASICSETTINGS_H
